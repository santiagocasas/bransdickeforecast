import os
import sys
import time
import copy
import argparse
import numpy as np
import tools


# -------------- Import parameters ------------------------------------------ #

# Read file ini and store information in a dictionary
parser = argparse.ArgumentParser()
parser.add_argument('input_file', type=str, help='Input file')
input_path = os.path.abspath(parser.parse_args().input_file)
input_content = tools.read_ini_file(input_path)

# Fix the common output path
output_path = os.path.abspath(input_content['paths']['output_path'])

# Initialize common output path
input_content, classy = tools.initialise(output_path, input_path)

# Read fiducial parameters
params_fiducial = copy.deepcopy(input_content['params_class'])

# Read z, k and epsilon values
z_min = float(input_content['params_precision']['z_min'])
z_max = float(input_content['params_precision']['z_max'])
z_num = int(input_content['params_precision']['z_num'])
z_spacing = input_content['params_precision']['z_spacing']
k_min = float(input_content['params_precision']['k_min_h/Mpc'])
k_max = float(input_content['params_precision']['k_max_h/Mpc'])
k_num = int(input_content['params_precision']['k_num'])
k_spacing = input_content['params_precision']['k_spacing']
abs_epsilons = input_content['params_precision']['abs_epsilons']
abs_epsilons = np.array(abs_epsilons.split(',')).astype(np.float)

# Read names of varying parameters together with their output folder name
params_varying = copy.deepcopy(input_content['params_varying'])

# Read file names that will be stored in each output folder
file_names = copy.deepcopy(input_content['output_file_names'])


# -------------- Main body -------------------------------------------------- #

# Ranges of k (in h/Mpc) and z to be used to sample background and perturbation
# and epsilon values considered to vary parameters around fiducial
z_range = tools.create_array(z_min, z_max, z_num, z_spacing)
k_range = tools.create_array(k_min, k_max, k_num, k_spacing)
epsilons = np.hstack((-abs_epsilons, abs_epsilons))

# Create List of cases to output to iterate over it
output_cases = tools.get_output_cases(list(params_varying.keys()), epsilons)

for nc, case in enumerate(output_cases):

    print('Starting {}, run {} of {}.'.format(case, nc+1, len(output_cases)))
    sys.stdout.flush()
    time_start = time.time()

    param_to_update = case[0]
    epsilon = float(case[1])

    params_case = tools.update_parameters(
        params_fiducial,
        param_to_update,
        epsilon)

    params_case = tools.move_class_param_in_array(
        params_case,
        param_name='wBD',
        array_name='parameters_smg',
        position=1)

    path_case = tools.create_output_path(
        output_path,
        param_to_update,
        epsilon,
        name_to_file=params_varying)

    if len(os.listdir(path_case)) != 0:
        print('----> Skipping, output already exists!')
        continue

    # Replace Omega_m with Omega_cdm
    params_list = params_case.keys()
    if 'Omega_m' in params_list and 'Omega_cdm' not in params_list:
        params_case['Omega_cdm'] = tools.Omega_m_to_Omega_cdm(
            classy.Class(),
            params_case)
        params_case.pop('Omega_m')

    # Run Class
    cosmo = classy.Class()
    cosmo.set(params_case)
    cosmo.compute()

    tools.savetxt(os.path.join(path_case, file_names['z']), z_range)
    tools.savetxt(os.path.join(path_case, file_names['k']), k_range)

    Hz = np.array([cosmo.Hubble(z) for z in z_range])
    DAz = np.array([cosmo.angular_distance(z) for z in z_range])
    tools.savetxt(os.path.join(path_case, file_names['H']), Hz)
    tools.savetxt(os.path.join(path_case, file_names['DA']), DAz)

    s8 = np.array([tools.get_sigma8_at_z(cosmo, z) for z in z_range])
    tools.savetxt(os.path.join(path_case, file_names['s8']), s8)

    Pk_lin = tools.get_Pzk(cosmo, z_range, k_range, nonlinear=False)
    tools.savetxt(os.path.join(path_case, file_names['Pl']), Pk_lin)

    # If nonlinear not requested, output linear PS
    nonlinear = 'non linear' in params_case
    Pk_nl = tools.get_Pzk(cosmo, z_range, k_range, nonlinear=nonlinear)
    tools.savetxt(os.path.join(path_case, file_names['Pn']), Pk_nl)

    Dzk = tools.get_Dzk(cosmo, z_range, k_range, nonlinear=False)
    tools.savetxt(os.path.join(path_case, file_names['D']), Dzk)

    fzk = tools.get_fzk(cosmo, z_range, k_range, nonlinear=False)
    tools.savetxt(os.path.join(path_case, file_names['f']), fzk)

    time_end = time.time()
    print('----> Finished in {:.2f} seconds!'.format(time_end-time_start))
    sys.stdout.flush()
