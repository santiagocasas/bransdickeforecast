import os
import sys
import copy
import configparser
import json
import random
import numpy as np
import matplotlib.pyplot as plt
import subprocess as sp
from shutil import copyfile


def read_ini_file(path):
    config = configparser.ConfigParser(
        inline_comment_prefixes=('#', ';'),
        empty_lines_in_values=False
    )
    config.optionxform = str
    with open(path) as fn:
        u = fn.read()
        config.read_string(u)
    content = json.loads(json.dumps(config._sections))
    return content


def initialise(output_path, input_path):
    tmp = read_ini_file(input_path)
    class_version = ''
    common_file_path = os.path.join(
        tmp['paths']['class_path'], 'include', 'common.h')
    with open(common_file_path, 'r') as common_file:
        for line in common_file:
            if line.find('_VERSION_') != -1:
                class_version = line.split()[-1].replace('"', '')
                break
    git_version = sp.Popen(
        ["git", "rev-parse", "HEAD"],
        cwd=tmp['paths']['class_path'],
        stdout=sp.PIPE,
        stderr=sp.PIPE).communicate()[0].strip()
    git_branch = sp.Popen(
        ["git", "rev-parse", "--abbrev-ref", "HEAD"],
        cwd=tmp['paths']['class_path'],
        stdout=sp.PIPE,
        stderr=sp.PIPE).communicate()[0].strip()
    bd_git_version = sp.Popen(
        ["git", "rev-parse", "HEAD"],
        cwd=os.getcwd(),
        stdout=sp.PIPE,
        stderr=sp.PIPE).communicate()[0].strip()
    bd_git_branch = sp.Popen(
        ["git", "rev-parse", "--abbrev-ref", "HEAD"],
        cwd=os.getcwd(),
        stdout=sp.PIPE,
        stderr=sp.PIPE).communicate()[0].strip()
    if not os.path.isdir(output_path):
        os.makedirs(output_path)
    if os.path.isfile(os.path.join(output_path, 'params.ini')):
        print('INI: found ini file at '+output_path+'. Using this instead!')
        ini_file = os.path.join(output_path, 'params.ini')
    else:
        ini_file = input_path
        head = '# BransDickeForecast GIT branch {}\n'.format(bd_git_branch)
        head += '# BransDickeForecast GIT version {}\n'.format(bd_git_version)
        head += '# CLASS version {}\n'.format(class_version)
        head += '# CLASS GIT branch {}\n'.format(git_branch)
        head += '# CLASS GIT version {}\n\n'.format(git_version)
        copyfile(ini_file, os.path.join(output_path, 'params.ini'))
        with open(os.path.join(output_path, 'params.ini'), 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(head.rstrip('\r') + '\n' + content)
    input_content = read_ini_file(ini_file)
    # Import Class from the desired path
    classy_path = find_classy_path(input_content['paths']['class_path'])
    sys.path.insert(1, classy_path)
    import classy  # noqa: E402
    return input_content, classy


def warn(message):
    print('WARNING: {}'.format(message))
    return


def savetxt(fname, array):
    np.savetxt(fname, array, delimiter='\t')
    return


def create_array(start, stop, num, spacing):
    if spacing == 'linear':
        array = np.linspace(start, stop, num)
    elif spacing == 'log':
        array = np.logspace(np.log10(start), np.log10(stop), num)
    else:
        raise ValueError('Spacing not recognized!')
    return array


def find_classy_path(root):
    classy_path = ''
    for elem in os.listdir(os.path.join(root, "python", "build")):
        if elem.find("lib.") != -1:
            classy_path = os.path.join(root, "python", "build", elem)
            break
    return os.path.abspath(classy_path)


def get_output_cases(params, epsilons):
    out = np.array(np.meshgrid(params, epsilons)).T.reshape(-1, 2)
    out = np.insert(out, 0, ['fiducial', '0.'], axis=0)
    return out


def add_epsilon(fiducial, epsilon):
    if fiducial == 0.:
        return fiducial+epsilon
    else:
        return fiducial*(1.+epsilon)


def update_parameters(params_in, param_to_update, epsilon):
    params_out = copy.deepcopy(params_in)
    if param_to_update != 'fiducial':
        value = float(params_in[param_to_update])
        params_out[param_to_update] = str(add_epsilon(value, epsilon))
    return params_out


def move_class_param_in_array(params_in, param_name, array_name, position):
    params_out = copy.deepcopy(params_in)
    try:
        array_values = params_out[array_name].split(',')
        param_value = params_out[param_name]
    except KeyError:
        warn('array {} and/or parameter {} not found!'.format(
            array_name,
            param_name))
        return params_out
    array_values[position] = str(param_value)
    params_out[array_name] = ','.join(array_values)
    params_out.pop(param_name)
    return params_out


def create_output_path(root, param_name, epsilon, name_to_file=None):
    f_param = param_name
    if name_to_file and param_name != 'fiducial':
        f_param = name_to_file[param_name]
    if epsilon > 0:
        f_pm = 'pl'
    elif epsilon < 0:
        f_pm = 'mn'
    else:
        f_pm = ''
    if epsilon == 0.:
        f_eps = '0'
    else:
        f_eps = '{:1.3f}'.format(abs(epsilon)).replace('.', 'p')
    f_name = '{}_{}_eps_{}'.format(f_param, f_pm, f_eps).replace('__', '_')
    output_path = os.path.join(root, f_name)
    if not os.path.isdir(output_path):
        os.makedirs(output_path)
    return output_path


def get_sigma8_at_z(cosmo, z):
    h = cosmo.h()
    sigma8 = cosmo.sigma(R=8./h, z=z)
    return sigma8


def growthrate_at_z_and_k(cosmo, z, k, nonlinear=False):
    dz = 0.005
    if nonlinear:
        cosmopk = cosmo.pk
    else:
        cosmopk = cosmo.pk_lin
    Pz0 = cosmopk(k, z)
    Pz1 = cosmopk(k, z+dz)
    Pz2 = cosmopk(k, z+2.*dz)
    f = -0.5*(1+z)*(-3.*np.log(Pz0)+4.*np.log(Pz1)-np.log(Pz2))/(2.*dz)
    return f


def get_Pzk(cosmo, z_vec, k_vec, nonlinear):
    # k (h/Mpc) -> k (1/Mpc)
    k_vec_1_Mpc = k_vec*cosmo.h()
    matterPk = cosmo.get_pk_array(
        k_vec_1_Mpc,
        z_vec,
        len(k_vec_1_Mpc),
        len(z_vec),
        nonlinear=nonlinear)
    matterPk = np.reshape(matterPk, (len(k_vec_1_Mpc), -1), 'F').T
    # Pk (Mpc^3) -> Pk (Mpc^3/h^3)
    matterPk = matterPk*cosmo.h()**3
    return matterPk


def get_Dzk(cosmo, z_vec, k_vec_h_Mpc, nonlinear=False):
    # Here k_vec is in (h/Mpc) and transformed in (1/Mpc) in get_Pzk
    Pzk0 = get_Pzk(cosmo, np.array([0.]), k_vec_h_Mpc, nonlinear=nonlinear)[0]
    Pzk = get_Pzk(cosmo, z_vec, k_vec_h_Mpc, nonlinear=nonlinear)
    Dzk = np.sqrt(Pzk/Pzk0)
    return Dzk


def get_fzk(cosmo, z_vec, k_vec, nonlinear=False):
    """
    This gives excellent agreement with the
    np.gradient function applied on Dzk.
    It is more accurate at the boundaries
    as it calculates ad-hoc extra points.
    """
    # k (h/Mpc) -> k (1/Mpc)
    k_vec_1_Mpc = k_vec*cosmo.h()
    fzk = np.zeros((len(z_vec), len(k_vec_1_Mpc)))
    for nz, z in enumerate(z_vec):
        for nk, k in enumerate(k_vec_1_Mpc):
            fzk[nz, nk] = growthrate_at_z_and_k(cosmo, z, k, nonlinear=False)
    return fzk


def Omega_m_to_Omega_cdm(cosmo, params_in):
    params_out = copy.deepcopy(params_in)
    pert_keys = ['output', 'lensing', 'non linear', 'sigma8', 'P_k_max_h/Mpc',
                 'z_max_pk', 'modes', 'l_max_scalars']
    for key in pert_keys:
        try:
            params_out.pop(key)
        except KeyError:
            pass
    Omega_m_wanted = float(params_out['Omega_m'])
    params_out.pop('Omega_m')
    params_out['Omega_cdm'] = Omega_m_wanted-float(params_out['Omega_b'])
    cosmo.set(params_out)
    cosmo.compute()
    Omega_cdm_value = Omega_m_wanted-(cosmo.Omega_m()-params_out['Omega_cdm'])
    return Omega_cdm_value


def plot_1D(data, models, x, y, xscale='linear',  yscale='linear',
            abs=False, threshold_diff=None, legend=False, title=False):
    plt.figure()
    for m in models:
        do_plot = False
        if threshold_diff and np.abs(data[m][y]).max() >= threshold_diff:
            do_plot = True
        elif not threshold_diff:
            do_plot = True
        if abs:
            xd, yd = data[m][x], np.abs(data[m][y])
        else:
            xd, yd = data[m][x], data[m][y]
        if do_plot:
            plt.plot(xd, yd, label=m)
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.xlabel(x)
    plt.ylabel(y)
    if title:
        plt.title(title)
    if legend:
        plt.legend()
    # plt.show()


def slice_2D(data, models, x, y, method='random'):
    """
    method: random, max_diff, value
    """
    if x == 'z':
        aux = 'k'
    elif x == 'k':
        aux = 'z'
    ytab = {}
    for m in models:
        if len(data[models[0]][x]) == len(data[models[0]][y]):
            ytab[m] = data[m][y].T
        else:
            ytab[m] = data[m][y]
    try:
        aux_val = float(method)
        tmp = np.abs(data[models[0]][aux]-aux_val)
        idx = np.where(tmp == tmp.min())[0][0]
    except ValueError:
        if method == 'random':
            idx = random.randrange(0, ytab[models[0]].shape[0])
        if method == 'max_diff':
            tmp = np.zeros(data[models[0]][aux].shape)
            for m in models:
                tmp += np.abs(np.nan_to_num(ytab[m], nan=0.)).sum(axis=1)
            idx = np.where(tmp == tmp.max())[0][0]

    idata = {}
    for m in models:
        idata[m] = {
            x: data[m][x],
            y: ytab[m][idx],
        }
    title = '{} = {}'.format(aux, data[models[0]][aux][idx])
    return idata, title


def plot_2D(data, models, x, y, method='random', **kwargs):

    idata, title = slice_2D(data, models, x, y, method=method)

    plot_1D(idata, models, x, y, title=title, **kwargs)


def find_nans(data, models):
    print('Outputs with nans:')
    for m in models:
        for key in data[m].keys():
            if np.isnan(data[m][key]).any():
                print('----> Model: {}. Observable: {}'.format(m, key))
