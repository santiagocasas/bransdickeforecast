import os
import argparse
import warnings
import numpy as np
import tools
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore",
                        message="invalid value encountered in true_divide")


# Parse input folder
parser = argparse.ArgumentParser()
parser.add_argument('input_folder', type=str, help='Input folder')
input_path = os.path.abspath(parser.parse_args().input_folder)

# Read data
data = {}
models = []
for dir in os.listdir(input_path):
    abs_dir = os.path.join(input_path, dir)
    if os.path.isdir(abs_dir) and len(os.listdir(abs_dir)) != 0:
        models.append(os.path.split(abs_dir)[1])
        data[models[-1]] = {
            'z': np.loadtxt(os.path.join(abs_dir, 'z_values_list.txt')),
            'k': np.loadtxt(os.path.join(abs_dir, 'k_values_list.txt')),
            'H': np.loadtxt(os.path.join(abs_dir, 'background_Hz.dat')),
            'DA': np.loadtxt(os.path.join(abs_dir, 'background_DAz.dat')),
            'D': np.loadtxt(os.path.join(abs_dir, 'D_Growth-zk.dat')),
            'f': np.loadtxt(os.path.join(abs_dir, 'f_GrowthRate-zk.dat')),
            's8': np.loadtxt(os.path.join(abs_dir, 'sigma8-z.dat')),
            'Pl': np.loadtxt(os.path.join(abs_dir, 'Plin-zk.dat')),
            'Pn': np.loadtxt(os.path.join(abs_dir, 'Pnonlin-zk.dat')),
        }

# Calculate rel_diffs
fid = 'fiducial_eps_0'
diff = {}
models_no_fid = [x for x in models if x != fid]
for m in models_no_fid:
    diff[m] = {
        'z': data[m]['z'],
        'k': data[m]['k'],
        'H': data[m]['H']/data[fid]['H']-1.,
        'DA': data[m]['DA']/data[fid]['DA']-1.,
        'D': data[m]['D']/data[fid]['D']-1.,
        'f': data[m]['f']/data[fid]['f']-1.,
        's8': data[m]['s8']/data[fid]['s8']-1.,
        'Pl': data[m]['Pl']/data[fid]['Pl']-1.,
        'Pn': data[m]['Pn']/data[fid]['Pn']-1.,
    }


tools.find_nans(data, models)

# Plot - H
tools.plot_1D(data, models_no_fid, 'z', 'H',
              threshold_diff=None, legend=False, abs=False)
tools.plot_1D(diff, models_no_fid, 'z', 'H',
              threshold_diff=None, legend=False, abs=True)
plt.show()

# Plot - DA
tools.plot_1D(data, models_no_fid, 'z', 'DA',
              threshold_diff=None, legend=False, abs=False)
tools.plot_1D(diff, models_no_fid, 'z', 'DA',
              threshold_diff=None, legend=False, abs=True)
plt.show()

# Plot - D
tools.plot_2D(data, models_no_fid, 'z', 'D', method=10,
              threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'z', 'D', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
tools.plot_2D(data, models_no_fid, 'k', 'D', xscale='log', method=3.,
              threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'k', 'D', xscale='log', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
plt.show()

# Plot - f
tools.plot_2D(data, models_no_fid, 'z', 'f', method=10,
              threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'z', 'f', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
tools.plot_2D(data, models_no_fid, 'k', 'f', xscale='log', method=0.,
              threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'k', 'f', xscale='log', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
plt.show()

# Plot - s8
tools.plot_1D(data, models_no_fid, 'z', 's8',
              threshold_diff=None, legend=False, abs=False)
tools.plot_1D(diff, models_no_fid, 'z', 's8',
              threshold_diff=None, legend=False, abs=True)
plt.show()

# Plot - Pl
tools.plot_2D(data, models_no_fid, 'z', 'Pl', method=10.,
              threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'z', 'Pl', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
tools.plot_2D(data, models_no_fid, 'k', 'Pl', xscale='log', yscale='log',
              method=0., threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'k', 'Pl', xscale='log', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
plt.show()

# Plot - Pn
tools.plot_2D(data, models_no_fid, 'z', 'Pn', method=10.,
              threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'z', 'Pn', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
tools.plot_2D(data, models_no_fid, 'k', 'Pn', xscale='log', yscale='log',
              method=0., threshold_diff=None, legend=False, abs=False)
tools.plot_2D(diff, models_no_fid, 'k', 'Pn', xscale='log', method='max_diff',
              threshold_diff=None, legend=False, abs=True)
plt.show()
